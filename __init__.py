import os
from Robot import Robot

if __name__ == "__main__":
    print("LIMPIA CASAS")

    r = input("\nNombre del Robot: ")
    robot = Robot(r)

    while True:
        if robot.nivel_bateria <= 0:
            print("Me quede sin bateria...")
            break

        print("\n\nMenu - Robot - " + robot.nombre)
        print("1. Barrer")
        print("2. Fregar")
        print("3. Hablar")
        print("4. Ver nivel de bateria")
        print("5. Regar plantas")
        print("6. Cargar bateria")
        print("7. Salir")

        try:
            o = int(input("\nOpcion: \n\n"))
        except:
            o = -15
        if o == 1:
            if robot.nivel_bateria<30:
                print("Por favor recargue la bateria")
            else:	
                print(robot.barrer())
        elif o == 2:
            if robot.nivel_bateria<50:
                print("Por favor recargue la bateria")
            else:	
                print(robot.fregar())
        elif o == 3:
            if robot.nivel_bateria<20:
                print("Por favor recargue la bateria")
            else:	
                print(robot.hablar())
        elif o == 4:
            print(robot.nivel_bateria)
        elif o == 5:
            if robot.nivel_bateria<10:
                print("Por favor recargue la bateria")
            else:
                print(robot.regarplantas())
        elif o == 6:
            print(robot.cargarbateria())		
        elif o == 7:
            print("Ciao")
            break
        else:
            print("Opcion invalida")
			
			
              

