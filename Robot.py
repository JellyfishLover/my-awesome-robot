import os
class Robot:
    version = 1.0

    def __init__(self, nombre):
        self.nombre = nombre
        self.nivel_bateria = 100

    def barrer(self):
        self.nivel_bateria -= 30
        return "\nEstoy barriendo..."

    def fregar(self):
        self.nivel_bateria = self.nivel_bateria - 50
        return "\nAgh... debo fregar..."

    def hablar(self):
        self.nivel_bateria -= 20
        print("\nBla bla bla bla bla!")
	
    def regarplantas(self):
        self.nivel_bateria -= 10
        return("Que bonitas plantas debo regar ^.^")

    def cargarbateria(self):
        while self.nivel_bateria<100:	
            print("El nivel de bateria es: " + str(self.nivel_bateria) + "% \n") 
            self.nivel_bateria += 1 
            print("\n Cargando...\n" + str(self.nivel_bateria) )
            os.system('cls')			
        return(self.nombre + " esta cargado!")
		